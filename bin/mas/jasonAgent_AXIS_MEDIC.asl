debug(3).

// Name of the manager
manager("Manager").
liderSupremo("").
// Team of troop.
team("AXIS").
// Type of troop.
type("CLASS_MEDIC").
mi_nombre("").
liderMedico("").
listaPosMedicos([]).
num_pos_medicos(0).


{ include("jgomas.asl") }



// Plans


/*******************************
*
* Actions definitions
*
*******************************/

/////////////////////////////////
//  GET AGENT TO AIM 
/////////////////////////////////  
/**
 * Calculates if there is an enemy at sight.
 *
 * This plan scans the list <tt> m_FOVObjects</tt> (objects in the Field
 * Of View of the agent) looking for an enemy. If an enemy agent is found, a
 * value of aimed("true") is returned. Note that there is no criterion (proximity, etc.) for the
 * enemy found. Otherwise, the return value is aimed("false")
 *
 * <em> It's very useful to overload this plan. </em>
 *
 */
+!get_agent_to_aim
    <-  ?debug(Mode); if (Mode<=2) { .println("Looking for agents to aim."); }
        ?fovObjects(FOVObjects);
        .length(FOVObjects, Length);
        
        ?debug(Mode); if (Mode<=1) { .println("El numero de objetos es:", Length); }
        
        if (Length > 0) {
		    +bucle(0);
    
            -+aimed("false");
    
            while (aimed("false") & bucle(X) & (X < Length)) {
  
                //.println("En el bucle, y X vale:", X);
                
                .nth(X, FOVObjects, Object);
                // Object structure 
                // [#, TEAM, TYPE, ANGLE, DISTANCE, HEALTH, POSITION ]
                .nth(2, Object, Type);
                
                ?debug(Mode); if (Mode<=2) { .println("Objeto Analizado: ", Object); }
                
                if (Type > 1000) {
                    ?debug(Mode); if (Mode<=2) { .println("I found some object."); }
                } else {
                    // Object may be an enemy
                    .nth(1, Object, Team);
                    ?my_formattedTeam(MyTeam);
          
                    if (Team == 100) {  // Only if I'm AXIS
				
 					    ?debug(Mode); if (Mode<=2) { .println("Aiming an enemy. . .", MyTeam, " ", .number(MyTeam) , " ", Team, " ", .number(Team)); }
					    +aimed_agent(Object);
                        -+aimed("true");

                    }
                    
                }
             
                -+bucle(X+1);
                
            }
                     
        }

     -bucle(_).

        

/////////////////////////////////
//  LOOK RESPONSE
/////////////////////////////////
+look_response(FOVObjects)[source(M)]
    <-  //-waiting_look_response;
        .length(FOVObjects, Length);
        if (Length > 0) {
            ///?debug(Mode); if (Mode<=1) { .println("HAY ", Length, " OBJETOS A MI ALREDEDOR:\n", FOVObjects); }
        };    
        -look_response(_)[source(M)];
        -+fovObjects(FOVObjects);
        //.//;
        !look.
      
        
/////////////////////////////////
//  PERFORM ACTIONS
/////////////////////////////////
/**
 * Action to do when agent has an enemy at sight.
 *
 * This plan is called when agent has looked and has found an enemy,
 * calculating (in agreement to the enemy position) the new direction where
 * is aiming.
 *
 *  It's very useful to overload this plan.
 *
 */
+!perform_aim_action
    <-  // Aimed agents have the following format:
        // [#, TEAM, TYPE, ANGLE, DISTANCE, HEALTH, POSITION ]
        ?aimed_agent(AimedAgent);
        ?debug(Mode); if (Mode<=1) { .println("AimedAgent ", AimedAgent); }
        .nth(1, AimedAgent, AimedAgentTeam);
        ?debug(Mode); if (Mode<=2) { .println("BAJO EL PUNTO DE MIRA TENGO A ALGUIEN DEL EQUIPO ", AimedAgentTeam); }
        ?my_formattedTeam(MyTeam);


        if (AimedAgentTeam == 100) {
        
            .nth(6, AimedAgent, NewDestination);
            ?debug(Mode); if (Mode<=1) { .println("NUEVO DESTINO MARCADO: ", NewDestination); }
            //update_destination(NewDestination);
        }
        .
    
/**
 * Action to do when the agent is looking at.
 *
 * This plan is called just after Look method has ended.
 *
 * <em> It's very useful to overload this plan. </em>
 *
 */
+!perform_look_action .
/// <- ?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR PERFORM_LOOK_ACTION GOES HERE.") }.

/**
 * Action to do if this agent cannot shoot.
 *
 * This plan is called when the agent try to shoot, but has no ammo. The
 * agent will spit enemies out. :-)
 *
 * <em> It's very useful to overload this plan. </em>
 *
 */
+!perform_no_ammo_action .
/// <- ?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR PERFORM_NO_AMMO_ACTION GOES HERE.") }.

/**
 * Action to do when an agent is being shot.
 *
 * This plan is called every time this agent receives a messager from
 * agent Manager informing it is being shot.
 *
 * <em> It's very useful to overload this plan. </em>
 *
 */
+!perform_injury_action .
///<- ?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR PERFORM_INJURY_ACTION GOES HERE.") }.


/////////////////////////////////
//  SETUP PRIORITIES
/////////////////////////////////
/**  You can change initial priorities if you want to change the behaviour of each agent  **/
+!setup_priorities
    <-  +task_priority("TASK_NONE",0);
        +task_priority("TASK_GIVE_MEDICPAKS", 2000);
        +task_priority("TASK_GIVE_AMMOPAKS", 0);
        +task_priority("TASK_GIVE_BACKUP", 0);
        +task_priority("TASK_GET_OBJECTIVE",1000);
        +task_priority("TASK_ATTACK", 1000);
        +task_priority("TASK_RUN_AWAY", 1500);
        +task_priority("TASK_GOTO_POSITION", 750);
        +task_priority("TASK_WALKING_PATH", 750).   



/////////////////////////////////
//  UPDATE TARGETS
/////////////////////////////////
/**
 * Action to do when an agent is thinking about what to do.
 *
 * This plan is called at the beginning of the state "standing"
 * The user can add or eliminate targets adding or removing tasks or changing priorities
 *
 * <em> It's very useful to overload this plan. </em>
 *
 */
+!update_targets 
	<-	?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR UPDATE_TARGETS GOES HERE.") }.
	
	
/////////////////////////////////
//  CHECK MEDIC ACTION (ONLY MEDICS)
/////////////////////////////////
/**
 * Action to do when a medic agent is thinking about what to do if other agent needs help.
 *
 * By default always go to help
 *
 * <em> It's very useful to overload this plan. </em>
 *
 */
+!checkMedicAction
<-  -+medicAction(on).
// go to help


/////////////////////////////////
//  CHECK FIELDOPS ACTION (ONLY FIELDOPS)
/////////////////////////////////
/**
 * Action to do when a fieldops agent is thinking about what to do if other agent needs help.
 *
 * By default always go to help
 *
 * <em> It's very useful to overload this plan. </em>
 *
 */
+!checkAmmoAction
<-  -+fieldopsAction(on).
//  go to help




/////////////////////////////////
//  PERFORM_TRESHOLD_ACTION
/////////////////////////////////
/**
 * Action to do when an agent has a problem with its ammo or health.
 *
 * By default always calls for help
 *
 * <em> It's very useful to overload this plan. </em>
 *
 */
+!performThresholdAction
       <-
       
       ?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR PERFORM_TRESHOLD_ACTION GOES HERE.") }
       
       ?my_ammo_threshold(At);
       ?my_ammo(Ar);
       
       if (Ar <= At) { 
          ?my_position(X, Y, Z);
          
         .my_team("fieldops_AXIS", E1);
         //.println("Mi equipo intendencia: ", E1 );
         .concat("cfa(",X, ", ", Y, ", ", Z, ", ", Ar, ")", Content1);
         .send_msg_with_conversation_id(E1, tell, Content1, "CFA");
       
       
       }
       
       ?my_health_threshold(Ht);
       ?my_health(Hr);
       
       if (Hr <= Ht) { 
          ?my_position(X, Y, Z);
          
         .my_team("medic_AXIS", E2);
         //.println("Mi equipo medico: ", E2 );
         .concat("cfm(",X, ", ", Y, ", ", Z, ", ", Hr, ")", Content2);
         .send_msg_with_conversation_id(E2, tell, Content2, "CFM");

       }
       .
       
/////////////////////////////////
//  ANSWER_ACTION_CFM_OR_CFA
/////////////////////////////////


    
+cfm_agree[source(M)]
   <- ?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR cfm_agree GOES HERE.")};
      -cfm_agree.  

+cfa_agree[source(M)]
   <- ?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR cfa_agree GOES HERE.")};
      -cfa_agree.  

+cfm_refuse[source(M)]
   <- ?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR cfm_refuse GOES HERE.")};
      -cfm_refuse.  

+cfa_refuse[source(M)]
   <- ?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR cfa_refuse GOES HERE.")};
      -cfa_refuse.  



/////////////////////////////////
//  Initialize variables
/////////////////////////////////

// Vamos a sobreescribirlo
// Esto lo ejecuta el medico lider al recibir una peticion de ayuda

+cfm(X, Y, Z, Salud)[source(M)]
    <-
	
	// Soy el medico lider y me han pedido ayuda
	// Entonces verie si envio a alguien, y quien esta mas cerca
	.println("Necesitan ayuda");
		
	
	
	.my_team("medic_AXIS", E1);
	.length(E1,Num_medicos_total);
	.println("Num_medicos_total : " , Num_medicos_total);
	+posiciones_medicos(X,Z);
	//.wait(num_pos_medicos(NMedicos) & NMedicos < Num_medicos_total);
	
	while(num_pos_medicos(NMedicos) & NMedicos < Num_medicos_total){
		//.println(":) ", NMedicos);
	}
	-+num_pos_medicos(0);
	.println("wait acaba --------------------------------------------------------------------------------------------------");
	?listaPosMedicos(PosMedicosLista);
	.println("Pos medicos en lista: " , PosMedicosLista);
	// posisiones: [[AM1,"pos(37.72721730574748,0,38.457139053327296)"],[AM2,"pos(38.83784311805624,0,22.522488853162514)"],[AM3,"pos(39.68690594787321,0,33.79612585701312)"]]
	//.println("Llamo a pos cercana con " , PosMedicosLista, " X: " ,X , " Z ", Z);
	
	
		
	.pos_cercana(PosMedicosLista,X,Z,Medico);
	if(Medico == "tu"){
		+ayudar(M,X,Z);
		.println("Voy yo ");
	} else{
		// En medico tenemos la pos del mdico a enviar
		//.println("Enviamos a : " , Medico);
		//.println("Llamo a letra con " , M);
		.letra(M,"False",Mminus);
		.concat("ayudar(",Mminus,",",X,",",Z,")", Content);
		.println("Mensaje para ",Medico," que ayude --------------- : " , Content);
		.send_msg_with_conversation_id(Medico, tell, Content, "CFM");
		
	}
	
	-+state(standing);
	
	-cfm(_)[source(M)]
.


+ayudar(Mminus,X,Z) [source(Lider)]<-
	.letra(Mminus, "True",M);
	.println("Necesita mi ayuda ",Mminus, " enviando  a " , M );
	!add_task(task("TASK_GIVE_MEDICPAKS", M, pos(X, 0, Z), ""));
.

+!init
   <- 
   +objetivo("NONE","NONE");
   +estado("FREE");
		
   +posNuevaX(0);
   +posNuevaZ(0);
.  

+goto(X,Y,Z)[source(A)] <-
	?estado(Estado);
	//.println("Recibido un mensaje de tipo goto de ", A);
	if(Estado == "FREE"){
		-+objetivo(X,Z);
		// Tareas para hacer
		?tasks(Tasktodo);
		// Guardamos todas las tareas que teniamos que hacer
		+tareas(Tasktodo);
		// Eliminamos todas las tareas
		-+tasks([]);
		
		!add_task(task("TASK_GOTO_POSITION", A, pos(X, Y, Z), ""));
		-+estado("Fase1");
		-+state(standing);
		-goto(_,_,_)
	}
	
.

// Acato ordenes y apunto quien es mi nuevo lider, supremo. Viva el Lider
+nuevoOrdenMundial[source(A)]<-
	//.println("He recidibo un mensaje, ahora sigo las ordenes de mi nuevo lider supremo ", A);
	-+liderSupremo(A);
	-nuevoOrdenMundial(_);
.

// Plan que hace que seas el nuevo lider de medicos y se lo comunica a los demas
+soyNuevoLiderMedicos[source(A)] <-
	.my_name(M);
	-+liderMedico(M);
	.register( "JGOMAS", "liderMedico_AXIS");
	.my_team("AXIS", E1);
	//.println("Soy  lider de medicos");
	// Plan a ejecutar en el resto de subordinados
	.concat("aceptar_nuevo_lider_medicos", NuevaOrdenDeMedicosDelFenixDos);
	.send_msg_with_conversation_id(E1, tell, NuevaOrdenDeMedicosDelFenixDos, "INT");
	-soyNuevoLiderMedicos;
.

+aceptar_nuevo_lider_medicos[source(A)] <-
	-+liderMedico(A);
	-aceptar_nuevo_lider_medicos(_);
.

// Anade las posiciones que recibe a la lista
@u[atomic]
+guarda_pos(X,Y,Z) [source(A)] <-
	?listaPosMedicos(LPM);
	.concat("pos(",X,",",Y,",",Z,")", Pos);
	.println("Recibido, guardo ", Pos);
	.println("Esto " , [A,Pos]);
	.union([[A,Pos]],LPM, PosMedicosLista);
	-+listaPosMedicos(PosMedicosLista);
	?num_pos_medicos(NumPosMedicos);
	-+num_pos_medicos(NumPosMedicos + 1);
	?num_pos_medicos(ZZZZZ);
	.println("NumPosMedicos :  " , ZZZZZ);
	//-guarda_pos(_,_,_);
.       

// Manda mensaje para enviat tu posicion a quien se te la pregunte
+dame_pos [source(A)]<-
	.println("--------------------------------------------------------------Problema aqui, solo entra la primera la llamada, la segunda pasa--------------------------------------------------------------------");
	?my_position(X, Y, Z);
	.concat("guarda_pos(",X,",",Y,",",Z,")", Msg);
	.println("Vale ",A," Te envio esto ", Msg, " -------------------------------------------------------------------------------------- ");
	.send_msg_with_conversation_id(A, tell, Msg, "INT");
	//-dame_pos;
.

// Guarda en la varible listaPosMedicos una lista tal que asi
// [[AM1,"pos(35.483431953229584,0,44.293577456439394)"],[AM2,"pos(47.740634219521674,0,28.50461330630195)"]]    

+posiciones_medicos(X,Z) <-
	.my_team("medic_AXIS", E1);
	-+listaPosMedicos([]);
	.concat("dame_pos", Msg);
	.println("Dadme vuestras posiciones, medicos" , E1);
	.send_msg_with_conversation_id(E1, tell, Msg, "INT");
	// Espero un segundo
	//.wait(2000);

	
	
	//-posiciones_medicos(_,_);
.

+aceptar_nuevo_lider_fieldops[source(A)] <-
	-+liderFieldOps(A);
	//.println("Mi nuevo lider fieldops es " , A);
	-aceptar_nuevo_lider_fieldops(_);
.