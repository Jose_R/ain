tareas([]).
estado("Fase1").
soldados_formacion([]).
num_soldados_formacion(0).
debugMaquinaEstados("False").
cubrir([]).
baseY(0).
baseZ(0).
soldados([]).
posiciones_soldados_formacion([]).
pos_Aux([]).
pos_form_x(50).
pos_form_z(50).
debugUpdate("False").
creencia("").
/**
* Action to do when agent has an enemy at sight.
* 
* This plan is called when agent has looked and has found an enemy,
* calculating (in agreement to the enemy position) the new direction where
* is aiming.
*
*  It's very useful to overload this plan.
* 
*/
// No se disparan entre amigos, se mueven un poco
+!perform_aim_action
    <-  // Aimed agents have the following format:
        // [#, TEAM, TYPE, ANGLE, DISTANCE, HEALTH, POSITION ]
        ?aimed_agent(AimedAgent);
        .nth(1, AimedAgent, AimedAgentTeam);
        ?debug(Mode); if (Mode<=3) { 
        	//.println("BAJO EL PUNTO DE MIRA TENGO A ALGUIEN DEL EQUIPO ", AimedAgentTeam , " Es " , AimedAgent);
        }                                                                                                                                                                                                                   
        ?my_formattedTeam(MyTeam);


        if (AimedAgentTeam == 200) { // 200 = nazi
			
        	.nth(6, AimedAgent, NewDestination);
			//.nth(4, AimedAgent, PositionEnemy);
			NewDestination = pos(EnemyPosX, _, EnemyPosZ);
            //.println("NUEVO DESTINO a disparar DEBERIA SER: ", NewDestination, " en " , EnemyPosX, " ", EnemyPosZ, " voy a comprobar si puedo");
			?fovObjects(FOVObjects);
			.length(FOVObjects, Length);
			?my_position(MyX,_,MyZ);
			if (Length > 0) { 
				+bucle(0);
				while(bucle(Y) & (Y < Length) ){
					
					.nth(Y,FOVObjects,Object);
					.nth(1,Object,Team);
					.nth(6,Object,Position);
					//.print("Object Team ", Team , " Position " ,Position);
					Position = pos(X1, _, Z1);
					//.println("llamando a between con " ,X1, " Y ", Z1 , " - ");
					-aliadoEnMedio(_);
					.between(MyX,MyZ,X1,Z1,EnemyPosX,EnemyPosZ,AliadoEnMedio);
					if(AliadoEnMedio == "True"){
						//.println("Vale, me tengo que mover");
						!add_task(task(20000,"TASK_GOTO_POSITION",MyName,pos(MyX+3,0,MyY+3),""));
						//-+state(standing);
						break;
					}
					// Posibles valores de AliadoEnMEdio "True" "False", como String
					-+bucle(Y+1);
				}// fin bucle
				
			}
        }
 .
 
 // Manda mensaje para enviar tu posicion a quien se te la pregunte
+dame_pos [source(A)]<-
	//.println("--------------------------------------------------------------Problema aqui, solo entra la primera la llamada, la segunda pasa--------------------------------------------------------------------");
	?my_position(X, Y, Z);
	.concat("guarda_pos(",X,",",Y,",",Z,")", Msg);
	//.println("Vale ",A," Te envio esto ", Msg, " -------------------------------------------------------------------------------------- ");
	.send_msg_with_conversation_id(A, tell, Msg, "INT");
	//-dame_pos;
.


+goto(X,Y,Z)[source(A)] <-
	?estado(Estado);
	//.println("Recibido un mensaje de tipo goto de ", A);

	//-+objetivo(X,Z);
	// Tareas para hacer
	?tasks(Tasktodo);
	// Guardamos todas las tareas que teniamos que hacer
	-+tareas(Tasktodo);
	// Eliminamos todas las tareas
	-+tasks([]);
	
	!add_task(task(20000,"TASK_GOTO_POSITION",A, pos(X, Y, Z), ""));
	-+estado("goto");
	?tasks(TT);
	//.println("Task todo " , TT);
	-+state(standing);
	-goto(_,_,_)
	
	
.


+createForm(X,Z)<-
	//.print("Create form");
	.my_team("ALLIED", E1);
	//.println("Creating Form para ", E1);
//	?soldados(Soldados);
	// LPos = [TS0,"pos(152.29511371669045,0,155.19388367856962)", ...],
	// en LPSFormada esta la lista de posiciones a las que tienen que ir los soldados
	
	.concat("pos(",0,",",0,",",0,")", Pos);
	?posiciones_soldados_formacion(LPos);
	
	
	for(.member(I, E1)){
		?pos_Aux(Pos_aux);
		
		.union([[I,Pos]],Pos_aux, Lpos);
		
		-+pos_Aux(Lpos);
	}
	.my_name(MyName);
	?pos_Aux(Pos_aux);
	.union([[MyName,Pos]],Pos_aux, Lpos);
	-+pos_Aux(Lpos);
	?pos_Aux(Lpos);
	
	.length(Lpos,LL);
	//.println("Posiciones soldados: ", Lpos, " \n >>> Length ", LL );
	.formation(Lpos, 50, 50, "R", LPSFormada);
	.length(LPSFormada,Length);
	//.println("Posiciones formacion : " ,LPSFormada ,  " \n longitud " , Length);
	-+pos_Aux([]);
	+enviarPosiciones(LPSFormada);
	-create_form(_);
.

+enviarPosiciones(LPSoldados) [source(A)]<-
    
    .my_team("ALLIED", EJE);
    //.length(LPSoldados, LSoldados);
	//.println(">>>>>>>>>>>> Length " , LSoldados);
    +bucle(0);
    for(.member(M, EJE) ){
        
                 
            ?bucle(X);
            
            .nth(X, LPSoldados, XX);
            .nth(X+1, LPSoldados, ZZ);
            
			//println(X , " it X ", XX, " Z " , ZZ, " para " , M);
			
            .concat("goto(",XX,", 0, ",ZZ,")", Content);
            
            //.print(M ,": ", Content);
            
            .send_msg_with_conversation_id(M, tell, Content, "INT");
        
        
            -+bucle(X+2);
        
    }
  
    
    -enviarPosiciones(_,_,_);
    -bucle(_);
.

/*
//.print("+enviarPosiciones con ", LPSoldados);
    
    .my_team("ALLIED", E1);
    .length(LPSoldados, Length);
    +bucleando(0);
	//?bucle(X);
    for(.member(I, E1) &  bucleando(X)){
		//?bucle(X);
        .nth(X, LPSoldados, XX);
        .nth(X+1, LPSoldados, ZZ);
        .println(X , " it X ", XX, " Z " , ZZ, " para " , I);
		
        .concat("goto(",XX,",0,",ZZ,")", Content);
            
        .print("---------- Envio a ",I ,": ", Content);
            
        .send_msg_with_conversation_id(I, tell, Content, "INT");
		-+bucleando(X+2);
		?bucleando(X);
        .println("X antes de irme : " , X);
    }
	
    
    -enviarPosiciones(_);
    -bucle(_);
*/

+go_objective <-
	//.println("Voy hacia el objetivo con el go_objetive");
	//!add_task(task(1000,"TASK_GET_OBJECTIVE",MyName,pos(50,0,50),""));
	
	-+estado("objetivo");
.

+quietos <-
	-+tasks([]);
.

@he_llegado_atomic[atomic]
+he_llegado [source(A)]<-
	//.println("Ha legado ", A);
	?soldados_formacion(S);
	.union([A],S, SS);
	//.println("S: " , SS);
	-+soldados_formacion(SS);
	?num_soldados_formacion(Num);
	-+num_soldados_formacion(Num + 1);
.

//vuelve a basem no se usa
+cubridme [source(A)] <-
	//.println("Cubriendo a ", A);
    //	-+tasks([]);
	/*
	.drop_intention(fsm);
	!!fsm;*/
	
	
	//-+state(standing);
	-+cubrir(A);
	-+estado("cubrir");
	?tasks(TT);
	//.println("Tareas " , TT);
  
.

/*.println("Reinicio la fsm");
	.drop_all_intentions;
	!!fsm;
	.my_name(MyName);
	!add_task(task("TASK_GET_OBJECTIVE",MyName,pos(X,0,Z),""));*/
	
+!update_targets
	<-	
	?debugUpdate(DebugUpdate);
	?estado(Estado);
	.my_name(MyName);
	.my_team("ALLIED", E1);
	?debugMaquinaEstados(DebugME);
	.my_team("supremo_ALLIED", LideresSupremos);

	////---
	if(Estado == "goto"){
		if(DebugUpdate == "True"){
			.println("Estado ", Estado);
		}
		?tasks(TT);
		//.println("Task todo " , TT);
		-+estado("goto_acabado");
		
	}
	if(Estado == "goto_acabado"){
		if(DebugUpdate == "True"){
			.println("Estado ", Estado);
		}
		?liderSupremo(Supremo);
		.concat("he_","llegado",Mensaje);
		.send_msg_with_conversation_id(Supremo, tell, Mensaje, "INT");
	}
	if(Estado == "objetivo"){
		/*if(DebugUpdate == "True"){
			.println("Estado ", Estado);
		}*/
		?tareas(Tareas);
		-+tasks(Tareas);
		.drop_intention(fsm);
		!!fsm;
		?tasks(TT);
		//.println("Task todo en objetivo" , TT);
		-+estado("volver");
		-+state("non_standing");
		/*if (TT == []) {
			.println("Tengo la bandera --------------------------------------------------------------------------");
			
			-+estado("volver");
			
		} */
	}
	if(Estado == "volver"){// el que tiene la bandera 
		?tasks(TT);
		/*if(DebugUpdate == "True"){
			.println("Estado ", Estado);
		}*/
		?my_position(MyX,_,MyZ);
		?objective_position(ObjectiveX, ObjectiveY, ObjectiveZ);
		
		if(objectivePackTaken(on)){
	    	//.println("Task todo en ", Estado ," ",  TT);
			-+estado("cubrir");
            
            .my_team("ALLIED",E1);
    
            .concat("cubrid","me",Mensaje);
            .send_msg_with_conversation_id(E1, tell, Mensaje, "INT");

		}

	} 

	if(Estado == "cubrir"){
		if(DebugUpdate == "True"){
			.println("Estado ", Estado);
		}
		
		?tasks(Tascas);
		//.println("Task todo en ", Estado ," ",  Tascas);
		?baseX(X);
		?baseZ(Z);
		.my_name(MyName);
		!add_task(task(20000,"TASK_GOTO_POSITION",MyName,pos(X,0,Z),""));
		//.println("Tasks : " , Tascas);
		/*.drop_all_intentions;
		!!fsm;*/
		//?cubrir(Cubriendo);
		-+estado(" ");
		//.println("Cubriendo a ", Cubriendo);
	}
	////---
	if(LideresSupremos == []){ // lista vacia = Tu eres el supremo manda mas
		if(DebugME == "True"){
			.println("Maquina de estados del Lidersupremo, estado " , Estado );
		}
		if(Estado == "Fase1"){
			
			.concat("goto(30,0,30)", Content1);                                      
	//		.send_msg_with_conversation_id(E1, tell, Content1, "INT");
			?soldados(S);
			-+estado("Formacion");
			if(DebugME == "True"){
				.println("hola soldados ", S);
			}
			?tasks(Tasktodo);
			-+tareas(Tasktodo);
			// Eliminamos todas las tareas
			-+tasks([]);
			
			
		}
		if(Estado == "Formacion"){
			
			//.println("Formacion!");
			?pos_form_x(X_form);
			?pos_form_z(Z_form);
			+createForm(X_form, Z_form);
			//.println("LLamado a formacion");
			?pos_form_x(Xform);
			?pos_form_z(Zform);
			!add_task(task(20000,"TASK_GOTO_POSITION",MyName,pos(Xform,0,Zform),""));
			// esperar a que estén todos
			-+estado("Formando");
		}
		if(Estado == "Formando"){
			.my_team("ALLIED", Aliados);
			.length(Aliados,Num_total);
			?tasks(Tasktodo);
			while(num_soldados_formacion(NFormados) & NFormados < Num_total){
				//.println(":) ", NFormados);
				//.println("Task todo lider " , Tasktodo);
			}
			//.println("YA !!");
			//.wait(10000);
			-+num_soldados_formacion(0);
			
			-+estado("Formados");
			-+state(standing);
		}
		if(Estado == "Formados"){
			.my_team("ALLIED", E1);
			//.println("Formados --------------");
			.concat("qui" , "etos", Quietos);
			.send_msg_with_conversation_id(E1, tell, Quietos, "INT");
			.wait(5000);
			//.println("5 secs");
			
			.concat("go_","objective",Tarea);
			
			.send_msg_with_conversation_id(E1, tell, Tarea, "INT");
			.wait(7000);
			//-+estado("Atacando");
			+go_objective;
			//?tareas(Tasktodo);
			// Eliminamos todas las tareas
			//-+tasks(Tasktodo);
			//!add_task(task(20000,"TASK_GOTO_POSITION",MyName,pos(40,0,40),""));
		}
	}
.
	
	
+guardar_pos_base <-
	?my_position(X,_,Z);
	-+baseX(X);
	-+baseZ(Z);
.



/////////////////////////////////
//  GET AGENT TO AIM 
/////////////////////////////////  
/**
* Calculates if there is an enemy at sight.
* 
* This plan scans the list <tt> m_FOVObjects</tt> (objects in the Field
* Of View of the agent) looking for an enemy. If an enemy agent is found, a
* value of aimed("true") is returned. Note that there is no criterion (proximity, etc.) for the
* enemy found. Otherwise, the return value is aimed("false")
* 
* <em> It's very useful to overload this plan. </em>
* 
*/  
+!get_agent_to_aim
<-  ?debug(Mode); 

?fovObjects(FOVObjects);
.length(FOVObjects, Length);

?debug(Mode); if (Mode<=1) { .println("El numero de objetos es:", Length); }

if (Length > 0) {
    +bucle(0);

    -+aimed("false");
	// Vamos a hacer focus al enemigo con menos vida
	// la posicion (en fov) se guardara en la siguiente vriable
	+apunto("");
    +aimed_agent_foo(0);
    while (bucle(X) & (X < Length)) {
        //aimed("false") & 
        
        
        .nth(X, FOVObjects, Object);
        // Object structure
        // [#, TEAM, TYPE, ANGLE, DISTANCE, HEALTH, POSITION ]
        .nth(2, Object, Type);
        
		// --
		?aimed_agent_foo(NN);

		.nth(NN, FOVObjects, ObjectFoo);  
		//.println("Ojectfoo " , ObjectFoo);
		.nth(5, ObjectFoo, HealthFoo);
		//.println("healthfoo " , HealthFoo);
		// --
		
        ?debug(Mode); if (Mode<=2) { .println("Objeto Analizado: ", Object); }
        
        if (Type > 1000) {
            ?debug(Mode); if (Mode<=2) { .println("I found some object."); }
        } else {
            // Object may be an enemy
            .nth(1, Object, Team);
            ?my_formattedTeam(MyTeam);
            
            if (Team == 200) {  // Only if I'm ALLIED
				
                    //.println("NN : " , NN);
                // .print("Voy a disparar a ", Object);
				.nth(5, Object, Health);
				//.println("Comparo vida " , Health , " con " , HealthFoo);
				if(Health <= HealthFoo){
					+apunto(Object);
					-+aimed("true");
				}
                
                
            }
            
        }
        
        -+bucle(X+1);
        
    }
	?aimed_agent_foo(NN);
	if(aimed(Aimed) & Aimed == "true"){
		?apunto(Apunto);
    	//.println(" ---------------------- Acabo apuntando a " , Apunto , " -----------------------------" );
		+aimed_agent(Apunto);
	}
	-bucle(_);
    
}

-bucle(_).
