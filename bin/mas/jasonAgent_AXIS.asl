debug(3).

// Name of the manager
manager("Manager").

// Team of troop.
team("AXIS").
// Type of troop.
type("CLASS_SOLDIER").
liderSupremo("").
liderFieldOps("").
liderMedico("").
mi_nombre("").

// para las formaciones
listaPosSoldados([]).
num_pos_soldados(0).



tareas([]).
patrollingRadius(60).
acting(0).




pos_objetivo(pos(0,0,0)).




{ include("jgomas.asl") }


// Plans


/*******************************
*
* Actions definitions
*
*******************************/

/////////////////////////////////
//  GET AGENT TO AIM
///////////////////////////////  
/**
* Calculates if there is an enemy at sight.
* 
* This plan scans the list <tt> m_FOVObjects</tt> (objects in the Field
* Of View of the agent) looking for an enemy. If an enemy agent is found, a
* value of aimed("true") is returned. Note that there is no criterion (proximity, etc.) for the
* enemy found. Otherwise, the return value is aimed("false")
* 
* <em> It's very useful to overload this plan. </em>
* 
*/  
+!get_agent_to_aim
<-  ?debug(Mode); 

?fovObjects(FOVObjects);
.length(FOVObjects, Length);

?debug(Mode); if (Mode<=1) { .println("El numero de objetos es:", Length); }

if (Length > 0) {
    +bucle(0);

    -+aimed("false");
	// Vamos a hacer focus al enemigo con menos vida
	// la posicion (en fov) se guardara en la siguiente vriable
	+apunto("");
    +aimed_agent_foo(0);
    while (bucle(X) & (X < Length)) {
        //aimed("false") & 
        
        
        .nth(X, FOVObjects, Object);
        // Object structure
        // [#, TEAM, TYPE, ANGLE, DISTANCE, HEALTH, POSITION ]
        .nth(2, Object, Type);
        
		// --
		?aimed_agent_foo(NN);

		.nth(NN, FOVObjects, ObjectFoo);  
		//.println("Ojectfoo " , ObjectFoo);
		.nth(5, ObjectFoo, HealthFoo);
		//.println("healthfoo " , HealthFoo);
		// --
		
        ?debug(Mode); if (Mode<=2) { .println("Objeto Analizado: ", Object); }
        
        if (Type > 1000) {
            ?debug(Mode); if (Mode<=2) { .println("I found some object."); }
        } else {
            // Object may be an enemy
            .nth(1, Object, Team);
            ?my_formattedTeam(MyTeam);
            
            if (Team == 100) {  // Only if I'm ALLIED
				
                    //.println("NN : " , NN);
                // .print("Voy a disparar a ", Object);
				.nth(5, Object, Health);
				//.println("Comparo vida " , Health , " con " , HealthFoo);
				if(Health <= HealthFoo){
					+apunto(Object);
					-+aimed("true");
				}
                
                
            }
            
        }
        
        -+bucle(X+1);
        
    }
	?aimed_agent_foo(NN);
	if(aimed(Aimed) & Aimed == "true"){
		?apunto(Apunto);
    	//.println(" ---------------------- Acabo apuntando a " , Apunto , " -----------------------------" );
		+aimed_agent(Apunto);
        
        .my_name(ME);
        ?liderSupremo(LDR);
            
       
      //  .print(XX, " ", ZZ);
        if ( ME == LDR) {
            .print("ID A PELEAR ALLI");
            .nth(6, Apunto, Pos);
            Pos = pos(XX,0,ZZ);
            
            +formadEn(XX,ZZ,0);
           // .print(XX, " ", ZZ);
            
            
        }
	}
	-bucle(_);
    
}

-bucle(_).
        

/////////////////////////////////
//  LOOK RESPONSE
/////////////////////////////////
+look_response(FOVObjects)[source(M)]
    <-  //-waiting_look_response;
        .length(FOVObjects, Length);
        if (Length > 0) {
            ///?debug(Mode); if (Mode<=1) { .println("HAY ", Length, " OBJETOS A MI ALREDEDOR:\n", FOVObjects); }
        };    
        -look_response(_)[source(M)];
        -+fovObjects(FOVObjects);
        //.//;
        !look.
      
        
/////////////////////////////////
//  PERFORM ACTIONS
/////////////////////////////////
/**
 * Action to do when agent has an enemy at sight.
 *
 * This plan is called when agent has looked and has found an enemy,
 * calculating (in agreement to the enemy position) the new direction where
 * is aiming.
 *
 *  It's very useful to overload this plan.
 * 
 */

+!perform_aim_action
 <-  // Aimed agents have the following format:
        // [#, TEAM, TYPE, ANGLE, DISTANCE, HEALTH, POSITION ]
        ?aimed_agent(AimedAgent);
        ?debug(Mode); if (Mode<=1) { .println("AimedAgent ", AimedAgent); }
        .nth(1, AimedAgent, AimedAgentTeam);
        ?debug(Mode); if (Mode<=2) { .println("BAJO EL PUNTO DE MIRA TENGO A ALGUIEN DEL EQUIPO ", AimedAgentTeam); }
        ?my_formattedTeam(MyTeam);


        if (AimedAgentTeam == 100) {
        
            .nth(6, AimedAgent, NewDestination);
            ?debug(Mode); if (Mode<=1) { .println("NUEVO DESTINO MARCADO: ", NewDestination); }
            //update_destination(NewDestination);
        }
        .
/**
 * Action to do when the agent is looking at.
 *
 * This plan is called just after Look method has ended.
 *
 * <em> It's very useful to overload this plan. </em>
 *
 */
+!perform_look_action .
/// <- ?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR PERFORM_LOOK_ACTION GOES HERE.") }.

/**
 * Action to do if this agent cannot shoot.
 *
 * This plan is called when the agent try to shoot, but has no ammo. The
 * agent will spit enemies out. :-)
 *
 * <em> It's very useful to overload this plan. </em>
 *
 */
+!perform_no_ammo_action .
/// <- ?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR PERFORM_NO_AMMO_ACTION GOES HERE.") }.

/**
 * Action to do when an agent is being shot.
 *
 * This plan is called every time this agent receives a messager from
 * agent Manager informing it is being shot.
 *
 * <em> It's very useful to overload this plan. </em>
 *
 */
+!perform_injury_action .
///<- ?debug(Mode); if (Mode<=1) { .println("YOUR CODE FOR PERFORM_INJURY_ACTION GOES HERE.") }.


/////////////////////////////////
//  SETUP PRIORITIES
/////////////////////////////////
/**  You can change initial priorities if you want to change the behaviour of each agent  **/
+!setup_priorities
    <-  +task_priority("TASK_NONE",0);
        +task_priority("TASK_GIVE_MEDICPAKS", 2000);
        +task_priority("TASK_GIVE_AMMOPAKS", 0);
        +task_priority("TASK_GIVE_BACKUP", 0);
        +task_priority("TASK_GET_OBJECTIVE",1000);
        +task_priority("TASK_ATTACK", 1000);
        +task_priority("TASK_RUN_AWAY", 1500);
        +task_priority("TASK_GOTO_POSITION", 750);
        +task_priority("TASK_WALKING_PATH", 750).   



/////////////////////////////////
//  UPDATE TARGETS
/////////////////////////////////
/**
 * Action to do when an agent is thinking about what to do.
 *
 * This plan is called at the beginning of the state "standing"
 * The user can add or eliminate targets adding or removing tasks or changing priorities
 *
 * <em> It's very useful to overload this plan. </em>
 *
 */
+!update_targets     
	<-
        //.print("UPDATE_TARGETS");
        
        ?acting(ACT);
        
        .my_name(ME);
         ?my_position(X,Y,Z);
        ?tasks(Tarea);
         ?objective(XX,YY,ZZ);
 
        
       //.print("Tareas: ", Tarea, " Estado: ", Est);
        
        if( ME == LDR ) {
             
            //.print("updateando estatus");
               
            if( ACT == 0) { 
            
               //./print("Vamos a proteger la bandera");
               
        
                -+patrollingRadius(5);
                
                +formadEn(XX,ZZ,0);
                
                
   
                -+acting(1);
            } else  {
                if ( ACT == 1 ) {
                  // .print("Ahora deberiamos orientarnos hacia el centro del mapa");
                    -+acting(4);
                   //./print("Vamos a mandar a todos los soldados en esa direccion y detenerlos casi al instante");
                    
                    .wait(1000);
                    
                    +formadEn(110,120, 20);
                    
                    
                    +darElAlto(7000);
                    .wait(200);
                    +patrullad;
                    
                     !add_task(task(500, "TASK_PATROLLING", ME, pos(XX, YY, ZZ), ""));  
                 }
                 

            }
        }
            // 10 = Patrol
        if (ACT == 10) {
            //.print("En Patrol");
             -+acting(-1);
   
          
            .drop_all_intentions;
            !!fsm;
            !add_task(task(1000,"TASK_PATROLLING", ME, pos(XX, YY, ZZ), "")); 
            
        }
            
            
            
        
            
           
            
         
     
 .

+patrullad <-
   //  .print("El lider manda que patrulleis");
    .my_team("backup_AXIS", E1);
         ////.println("Mi equipo intendencia: ", E1 );
    .concat("patrulla", Content1);
    
    .send_msg_with_conversation_id(E1, tell, Content1, "INT");   
    
      //./print(Taska);
    // -+state(standing);
     
     -patrullad
.

+patrulla[source(M)]
  <- // .print("Patrullo");
       //-+tasks([]);
       
       
       
       
       -+acting(10);
      
.


+darElAlto(T) <-
   //./print("Dare el alto en un instante!");
  
    .wait(T);
    .my_team("backup_AXIS", E1);
         ////.println("Mi equipo intendencia: ", E1 );
    .concat("alto", Content1);
    
    .send_msg_with_conversation_id(E1, tell, Content1, "CFA");   
        
    -+tasks([]);
     ?tasks(Taska);
      //./print(Taska);
    // -+state(standing);
     
     -darElAlto
 .
 
 +alto[source(M)]
  <-//./print("Mi fuhrer me ordena que me quede quieto y eso hago, mis tareas son....");
       -+tasks([]);
       ?tasks(Taska);
      //./print(Taska);
       -+state(standing);
       -+acting(0);
       
.
 

+formadEn(X, Z, C) <-
    +print("En formad");
    +formacion;
    
    .wait(1000);
    
    ?listaPosSoldados(LPS);
    //.print(LPS);
    
    
    //.print("LPS: ", LPS);
  //  .formacionDos(LPS, X, Z, C, "R", LPSFormada);
    .formation(LPS,X,Z,"R",LPSFormada);
    +enviarPosiciones(LPSFormada);
    
    
    
    //.print("VOY ALLA");
    !add_task(task(20000,"TASK_GOTO_POSITION",ME,pos(X,0,Z),""));
    //.print("ADOLF ME ESTOY MOVIENDO");
    
    
    
   -formadEn(X,Z,c);
.
	
	
/////////////////////////////////
//  CHECK MEDIC ACTION (ONLY MEDICS)
/////////////////////////////////
/**
 * Action to do when a medic agent is thinking about what to do if other agent needs help.
 *
 * By default always go to help
 *
 * <em> It's very useful to overload this plan. </em>
 *
 */
+!checkMedicAction
<-  -+medicAction(on).
// go to help


/////////////////////////////////
//  CHECK FIELDOPS ACTION (ONLY FIELDOPS)
/////////////////////////////////
/**
 * Action to do when a fieldops agent is thinking about what to do if other agent needs help.
 *
 * By default always go to help
 *
 * <em> It's very useful to overload this plan. </em>
 *
 */
+!checkAmmoAction
<-  -+fieldopsAction(on).
//  go to help



/////////////////////////////////
//  PERFORM_TRESHOLD_ACTION
/////////////////////////////////
/**
 * Action to do when an agent has a problem with its ammo or health.
 *
 * By default always calls for help
 *
 * <em> It's very useful to overload this plan. </em>
 *
 */
+!performThresholdAction
       <-

       
       ?my_ammo_threshold(At);
       ?my_ammo(Ar);
       
       if (Ar <= At) { 
          ?my_position(X, Y, Z);

         .my_team("fieldops_AXIS", E1);
         //.println("Mi equipo intendencia: ", E1 );
         .concat("cfa(",X, ", ", Y, ", ", Z, ", ", Ar, ")", Content1);
       //  .send_msg_with_conversation_id(E1, tell, Content1, "CFA");
       
       
       }
       
       ?my_health_threshold(Ht);
       ?my_health(Hr);
       
       if (Hr <= Ht) {
	       ////.println("Necesito ayuda, tengo poca vida! : tengo " , Ar);
		 //  +pedirAyudaMedica;
		   // Disminuimos el limite de pedir ayuda
		   -+my_health_threshold(Ht-50);
		   // Pedira, como maximo, dos veces
       }
.

+pedirAyudaMedica <-
	?my_position(X, Y, Z);
	?my_health(Hr);
	?liderMedico(MedicoLider);
	.concat("cfm(",X, ", ", Y, ", ", Z, ", ", Hr, ")", Content2); // cfm(X,Y,Z,Hr)
	//.println("Pido ayuda: " , Content2, " A " , MedicoLider);
	.send_msg_with_conversation_id(MedicoLider, tell, Content2, "CFM");
	
	-pedirAyudaMedica;
.
	
       
/////////////////////////////////
//  ANSWER_ACTION_CFM_OR_CFA
/////////////////////////////////

   
    
+cfm_agree[source(M)]
   <- ?debug(Mode); if (Mode<=1) { }//.println("YOUR CODE FOR cfm_agree GOES HERE.")};
      -cfm_agree.  

+cfa_agree[source(M)]
   <- ?debug(Mode); if (Mode<=1) {} //.println("YOUR CODE FOR cfa_agree GOES HERE.")};
      -cfa_agree.  

+cfm_refuse[source(M)]
   <- ?debug(Mode); if (Mode<=1) { }//.println("YOUR CODE FOR cfm_refuse GOES HERE.")};
      -cfm_refuse.  

+cfa_refuse[source(M)]
   <- ?debug(Mode); if (Mode<=1) { }//.println("YOUR CODE FOR cfa_refuse GOES HERE.")};
      -cfa_refuse.  



/////////////////////////////////
//  Initialize variables
/////////////////////////////////

+!init
   <-
   //.print("INIT");
   
   .my_name(M);
   //.print(M);
   
   .concat("",M,ME);
   //.print(ME == "TS0");
   
   if (ME == "TS0") {
        //.print("Soy lider");
        +proclamaYComunicarLiderSupremo;
        
        ?liderSupremo(LD);
        //.print(LD);
        
        .my_team("AXIS", E1);
        
        for(.member(I, E1)){
            
            //.print(I);
			////.println("I es ", I);
			if(I == "TF0"){
				+nuevoLiderFieldOps("TF0");
			}
			if(I == "TM0"){
				// sera para medico
				+nuevoLiderMedicos(I);
			}
		}

       
   }
   
  

	
. 
 
+enviarPosiciones(LPSoldados) <-
   //./print("+ENVIARPOSICIONES");
    
    
    .my_team("backup_AXIS", EJE);
     
    +bucle(0);
    for(.member(M, EJE) ){
        
        if ( M == "TS0") {
        } else {            
            //.print(M);    
            ?bucle(X);
            
            .nth(X, LPSoldados, XX);
            .nth(X+1, LPSoldados, ZZ);
            
            .concat("goto(",XX,", 0, ",ZZ,")", Content);
            
            .send_msg_with_conversation_id(M, tell, Content, "INT");
        
        
            -+bucle(X+2);
        }
    }
  
    ?tasks(Tasktodo);
		// Guardamos todas las tareas que teniamos que hacer
	+tareas(Tasktodo);
		// Eliminamos todas las tareas
	-+tasks([]);
    
    -enviarPosiciones(_,_,_);
    -bucle(_);
.
 
+goto(X,Y,Z)[source(A)]<-
	////.println("He recidibo un mensaje, ahora sigo las ordenes de mi nuevo lider supremo ", A);
	//.print("Me han llamado en el frente");
   //./print("roger lider");
   
    
   .print("ME MUEVO");
   
    // .print("Aqui viene check_pos : ", X, " ", Z);
    //.print(TA);
     -+tasks([]); 
    
    !add_task(task("TASK_GOTO_POSITION", A, pos(X, 0, Z), ""));
 
    ?tasks(TAS);
   //./print(TAS);
    //.print(TAS);
    -+state(standing);
    -+acting(0);
    
	-goto(_,_,_);
.
 

 
 
+formacion <-
    // //.print("+Formacion");
    -+listaPosSoldados([]);
    ?my_position(X,Y,Z);
    .concat("pos(",X,",",Y,",",Z,")", Pos);
    .my_name(M);
	.concat("",M,Me);
    
    -+listaPosSoldados([[Me,Pos]]);
    
    +posiciones_soldados(X, Z);
.    

+posiciones_soldados(X,Z) <-
    // //.print("+POSICIONES SOLDADOS");
	.my_team("backup_AXIS", E1);
	.concat("dame_pos", Msg);
	// //.println("Dadme vuestras posiciones, soldados" , E1);
	.send_msg_with_conversation_id(E1, tell, Msg, "INT");
	// //.print("mensajes enviados");
	
	//-posiciones_medicos(_,_);
.    

+dame_pos [source(A)]<-
	// //.println("--------------------------------------------------------------Problema aqui, solo entra la primera la llamada, la segunda pasa--------------------------------------------------------------------");
	?my_position(X, Y, Z);
    //.print("DANDO POSICION");
	.concat("guarda_pos(",X,",",Y,",",Z,")", Msg);
	// //.println("Roger ",A," Te envio esto ", Msg, " -------------------------------------------------------------------------------------- ");
	.send_msg_with_conversation_id(A, tell, Msg, "INT");
	//-dame_pos;
.

@u[atomic]
+guarda_pos(X,Y,Z) [source(A)] <-
    
    // //.print("+GUARDANDO POSICION");
	?listaPosSoldados(LPS);
	.concat("pos(",X,",",Y,",",Z,")", Pos);
	// //.println("Recibido ", A, ", guardo ", Pos);
	.union([[A,Pos]],LPS, PosSoldadosLista);
    //.print("Soy fuhrer y he recibdo una pos");
    //.print(PosSoldadosLista);
	-+listaPosSoldados(PosSoldadosLista);
	?num_pos_soldados(NumPosSoldados);
	-+num_pos_soldados(NumPosSoldados + 1);
	?num_pos_soldados(ZZZZZ);
	// //.println("NumPosSoldados :  " , ZZZZZ);
	//-guarda_pos(_,_,_);
. 

// Metodo que se lanza para autoproclamarte lider y comunicar al resto de que TU eres el lider
+proclamaYComunicarLiderSupremo <-
	.my_name(M);
	-+liderSupremo(M);
	.register( "JGOMAS", "supremo_AXIS");
	.my_team("AXIS", E1);
	// Plan a ejecutar en el resto de subordinados
	.concat("nuevoOrdenMundial", NuevoOrdenMundial);
	.send_msg_with_conversation_id(E1, tell, NuevoOrdenMundial, "INT");
	-proclamaYComunicarLiderSupremo;
.

// Acato ordenes y apunto quien es mi nuevo lider, supremo. Viva el Lider
+nuevoOrdenMundial[source(A)]<-
	// //.println("He recidibo un mensaje, ahora sigo las ordenes de mi nuevo fuhrer supremo ", A);
	-+liderSupremo(A);
	-nuevoOrdenMundial(_);
.

// Indicamos el nuevo lider de los fieldops. EL propio lider tambien recibe el mensaje.
+nuevoLiderFieldOps(N) <-
	-+liderFieldOps(N);
	.concat("soyNuevoLiderFieldOps", NuevoOrdenMundial);          
	// //.println("Le digo a  " ,N ," que es lider de fieldops del EJE Con el mensaje ", NuevoOrdenMundial);
	.send_msg_with_conversation_id(N, tell, NuevoOrdenMundial, "INT");
	-nuevoLiderFieldOps(_);
.

// Indicamos el nuevo lider de los medicos. EL propio lider tambien recibe el mensaje.
+nuevoLiderMedicos(N) <-
	-+liderMedico(N);
	.concat("soyNuevoLiderMedicos", NuevoOrdenDeLosMedicosDelFenix);          
	// //.println("Le digo a  " ,N ," que es lider de medicos del EJE Con el mensaje ", NuevoOrdenDeLosMedicosDelFenix);
	.send_msg_with_conversation_id(N, tell, NuevoOrdenDeLosMedicosDelFenix, "INT");
	-nuevoLiderMedicos(_);
.

+aceptar_nuevo_lider_medicos[source(A)] <-
	-+liderMedico(A);
	-aceptar_nuevo_lider_medicos(_);
.
