
package jason.stdlib;
import java.util.ArrayList;

import jade.domain.*;
import jade.domain.FIPAAgentManagement.*;
import jason.asSemantics.*;
import jason.asSyntax.*;
import jason.*;
import jason.infra.jade.JadeAgArch;
import java.lang.Math;
import java.util.logging.Logger;


public class pos_cercana extends DefaultInternalAction {

    private Logger logger = Logger.getLogger("JadeDF.mas2j."+register.class.getName());
	private boolean _DEBUG = false;

	/**
	 * Entradas en args: una lista del estilo, tu posicion X e Z y ultimo argumento para el resultado
	 // posisiones: [[AM1,"pos(37.72721730574748,0,38.457139053327296)"],[AM2,"pos(38.83784311805624,0,22.522488853162514)"],[AM3,"pos(39.68690594787321,0,33.79612585701312)"]]
	 // posX pos(1)
	 // PosZ pos(2)
	 @Return el agente mas cercano (solo su nombre)
	 * */
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
        //ts.getAg().getLogger().info("Ejecutando clase Between");
		Term nombreAgente = null;
		ListTerm vt;
        try{
			vt = (ListTerm)args[0];
		
			if (_DEBUG){
				System.out.println("-------------------------------");
				System.out.println(vt);
				System.out.println("lista de terminos -------------------------------");
			}
			ListTerm lt;
			Term nombre;
			Term pos;
			double distanciaMinima = 999999999;
			double distAux = -1;
			
			double[] db;
			// Mi posicion
			NumberTerm nt = (NumberTerm)args[1];
			double mi_x = nt.solve();
			nt = (NumberTerm)args[2];
			double mi_z = nt.solve();
			nt = (NumberTerm)args[2];
			for(Term t : vt){
				// t = [AM1,"pos(47.01320951783813,0,27.554735658514264)"]		
				lt = (ListTerm)t;
				if(_DEBUG) System.out.println(lt);
				nombre = lt.getTerm();
				pos = lt.getNext();
				if (_DEBUG){
					//System.out.println("Antes de llamar a getPos, pos: " + (((StringTerm)(((ListTermImpl)pos).get(0))).getString()) );
					System.out.println("Nombre " + nombre  + " Pos " + pos);
				}
				
				// en db tenemos la posX y la posZ
				db = getPos((((StringTerm)(((ListTermImpl)pos).get(0))).getString()));
				
				distAux = distanciaEntreDosPuntos(mi_x, mi_z, db[0], db[1]);
				if(distAux < distanciaMinima){
					distanciaMinima = distAux;
					nombreAgente = nombre;
				}
			}
	
			if(_DEBUG){
				System.out.println("Devuelvo como resultado el agente: " + nombreAgente	 + " a una distancia " + distanciaMinima);
			}
			
			return un.unifies(nombreAgente, args[3]);
		}catch(Exception e){
			System.out.println("lista vacia, vas tu!");
			//nombreAgente = StringTermImpl(String.valueOf("tu"));
			return un.unifies(new StringTermImpl("tu"), args[3]);
		}
    }
    
	//entrada "pos(47.01320951783813,0,27.554735658514264)" 
	//devuelve double[posX, posZ]
	double[] getPos(String pos){
		pos = pos.replaceAll("[^\\.0123456789]","-");
		
		String[] tokens = pos.split("-");
		if(_DEBUG)
			System.out.println("-----------------------Pos: " + pos);
		double[] res = new double[2];
		int i = 0;
		for(String str: tokens){
			if(_DEBUG)
				System.out.println("string: " + str);
			if(!str.isEmpty()){
				if(Double.parseDouble(str) != 0){ // es la X-> 0
					res[i] = Double.parseDouble(str);
					if(_DEBUG)
						System.out.println("Double to string: " + res[i]);
					i++;
				}
				
			}
			
			
		}
		return res;
	}
	 // devuelve la distancia entre dos puntos
	 double distanciaEntreDosPuntos(double MyX, double MyY, double EnX, double EnY){
		 return Math.sqrt( Math.pow((MyX - EnX),2) + Math.pow((MyY - EnY),2) );
		 
	 }
    
}
