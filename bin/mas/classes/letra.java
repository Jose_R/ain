
package jason.stdlib;
import java.util.ArrayList;

import jade.domain.*;
import jade.domain.FIPAAgentManagement.*;
import jason.asSemantics.*;
import jason.asSyntax.*;
import jason.*;
import jason.infra.jade.JadeAgArch;
import java.lang.Math;
import java.util.logging.Logger;


public class letra extends DefaultInternalAction {

    private Logger logger = Logger.getLogger("JadeDF.mas2j."+register.class.getName());
	private boolean _DEBUG = true;

	/**
	 * String a cambiar
	                                                                                                                                                  
	 	"True" para mayus, "False" para minus
	 - > Cambia el string dado de minusculas a mayusculas y vicerversa, lo guarda en el tercer arg.
	 * */                                                                                                                                                 
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
        //ts.getAg().getLogger().info("Ejecutando clase Between");

       
		if (_DEBUG){
			System.out.println("-------------------------------");
			System.out.println("lista de terminos -------------------------------" + args);
		}
		
		StringTerm str = (StringTerm)args[0];
		
		StringTerm queHacer = (StringTerm)args[1];
		if (_DEBUG) {
			System.out.println("Tengo ----------------------- "+str+" ------------------- con toString " + str.toString());
			System.out.println("segundo argumento : " + queHacer.toString());
		}
		String cad = str.toString();
		boolean b = queHacer.toString().equals("\"True\"");
		if (_DEBUG)
			System.out.println("Es true or false? " + b + " Con cadena " + cad);
		if(b){ // Pasar a mayus
			cad = cad.toUpperCase();
		} else { // minus
			cad = cad.toLowerCase();
			
		}
		// Todos los strign que nos pasan vienen con comillas (por el toString, ya que el getString no me iba..)
		// Antes las tratabamos, ahora las quitamos
		//cad = cad.substring(1,cad.length()-1);
		System.out.println("Devuelvo ----------------------- "+cad+" -------------------");
		str = new StringTermImpl(String.valueOf(cad));
        return un.unifies(str, args[2]);
    }
    

    
}
