
package jason.stdlib;


import jade.domain.*;
import jade.domain.FIPAAgentManagement.*;
import jason.asSemantics.*;
import jason.asSyntax.*;
import jason.*;
import jason.infra.jade.JadeAgArch;

import java.util.logging.Logger;


public class between extends DefaultInternalAction {

    private Logger logger = Logger.getLogger("JadeDF.mas2j."+register.class.getName());


	/**
	 * Entradas en args: 4 ints 1 variable
	 * PosX, PosY, PosX2, PosY2,PosXFinal,PosYFinal variableResultado
	 * */
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
        //ts.getAg().getLogger().info("Ejecutando clase Between");
		StringTerm type;
		String error = "La entrada ha de ser 6 ints y una variable para guardar el resultado";
        // comprobamos el numero de args
        if (args != null && args.length != 7) {
            ts.getAg().getLogger().info("Internal Action 'jason.stdlib.between' needs 7 arguments.");
            throw new JasonException("Internal Action 'jason.stdlib.between' needs 7 arguments.");

        } else {
            // primeros 4 argumentos han de ser numeros
            if (!args[0].isNumeric() || !args[1].isNumeric() || !args[2].isNumeric() || !args[3].isNumeric() || !args[4].isNumeric() || !args[5].isNumeric() || !args[6].isVar()) {
                ts.getAg().getLogger().info(error);
                System.out.println("Error");
                throw new JasonException(error);
            }else{
					System.out.println("Son numeros");
					
			}
			/*
            // Check that the first argument has value distinct of null value.
            StringTerm typeTerm = (StringTerm) args[0];
            String desiredType;
            StringTerm type;
            if (typeTerm != null) {
                desiredType = typeTerm.getString();

            } else {
                ts.getAg().getLogger().info(
                        "Incorrect value for 'type' argument in Internal Action 'jason.stdlib.get_type'.");
                throw new JasonException(
                        "Incorrect value for 'type' argument in Internal Action 'jason.stdlib.get_type'.");
            }
			*/
			NumberTerm nt = (NumberTerm)args[0];
			double a = nt.solve();
			nt = (NumberTerm)args[1];
			double b = nt.solve();
			nt = (NumberTerm)args[2];
			double c = nt.solve();
			nt = (NumberTerm)args[3];
			double d = nt.solve();
			nt = (NumberTerm)args[4];
			double e = nt.solve();
			nt = (NumberTerm)args[5];
			double f = nt.solve();
			if (isBetween(a,b,c,d,e,f) ){
				type = new StringTermImpl(String.valueOf("True"));
			}else{
				type = new StringTermImpl(String.valueOf("False"));
			}
			/*
            // Create the term with the result and unifies the result with the
            // second argument.
            if (desiredType.equalsIgnoreCase("CLASS_NONE")) {
                type = new StringTermImpl(String.valueOf(Constants.CLASS_NONE));
            } else if (desiredType.equalsIgnoreCase("CLASS_SOLDIER")) {
                type = new StringTermImpl(String.valueOf(Constants.CLASS_SOLDIER));
            } else if (desiredType.equalsIgnoreCase("CLASS_MEDIC")) {
                type = new StringTermImpl(String.valueOf(Constants.CLASS_MEDIC));
            } else if (desiredType.equalsIgnoreCase("CLASS_ENGINEER")) {
                type = new StringTermImpl(String.valueOf(Constants.CLASS_ENGINEER));
            } else if (desiredType.equalsIgnoreCase("CLASS_FIELDOPS")) {
                type = new StringTermImpl(String.valueOf(Constants.CLASS_FIELDOPS));
            } else {
                type = new StringTermImpl();
            }*/
		}

            return un.unifies(type, args[6]);
    }
    
    /**
      * MyX, MyY son mis coordenadas,
      * EnX, EnY son las del objetivo
      * PX, PY son las del punto a comprobar
      * devuelve false si el punto PY no esta en medio (ya sea por no
		estar en la recta, o bien por estar mas lejos que En)
      */
     boolean isBetween(double MyX, double MyY, double EnX, double EnY, double PX, double PY){

         double cof_x = EnY - MyY;
         double cof_y = EnX - MyX;
         //comprobamos si esta en la recta
         if(cof_x*PX - cof_x*MyX != cof_y*PY - cof_y*MyY)
             return false;
         //comprobamos que el punto este más lejos que el objetivo

         double dist_My_En = Math.sqrt((EnX-MyX)*(EnX-MyX)+(EnY-MyY)*(EnY-MyY));
         double dist_My_P =  Math.sqrt((PX-MyX)*(PX-MyX)+(PY-MyY)*(PY-MyY));
         if(dist_My_En < dist_My_P)
             return false;


         return true;
     }
    
}
