
package jason.stdlib;


import jade.domain.*;
import jade.domain.FIPAAgentManagement.*;
import jason.asSemantics.*;
import jason.asSyntax.*;
import jason.*;
import jason.infra.jade.JadeAgArch;

import java.util.logging.Logger;
import java.util.ArrayList;


public class formation extends DefaultInternalAction {

    private Logger logger = Logger.getLogger("JadeDF.mas2j."+register.class.getName());


	/**
	 * Entradas en args: lista de [TS0,"pos(152.29511371669045,0,155.19388367856962)", ...], POS X lider, POS Y lider (destino), tipo R o C, lista Salida.
     */
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
        ListTerm vt;
        ListTerm aux;       
        
        
        vt = (ListTerm)args[0];
        aux = (ListTerm)args[0];
        double X_dest = ((NumberTerm)args[1]).solve();
        double Y_dest = ((NumberTerm)args[2]).solve();
        Term tipo = args[3];
        //sacar la ecucacion de la r
        System.out.println(vt);

        double x_lider;
        double y_lider;
        double offset = 10;
        
        Term nombre;
        Term pos;
        
        double[] pos_aux;
        
        ArrayList<double []> lista_posiciones = new ArrayList<double []>();
        ArrayList<Term> lista_nombres = new ArrayList<Term>();
        
        
        for(Term t : vt) {
            ListTerm lt = (ListTerm)t;
            
            nombre = lt.getTerm();
            pos = lt.getNext();
                 
            // en db tenemos la posX y la posZ
            pos_aux = getPos((((StringTerm)(((ListTermImpl)pos).get(0))).getString()));
            lista_posiciones.add(pos_aux);
            lista_nombres.add(nombre);
        }
        
        double m = pendiente(lista_posiciones.get(0), X_dest, Y_dest);
        
        for(double [] d : lista_posiciones) {
            //System.out.println("X: "+d[0]+"  Y:"+d[1]);
        }
        
        //System.out.println("Lista de nombres: "+ lista_nombres);
        
        //vamos a calculcar tantos puntos como soldados haya - 1. El lider va al punto P
        
        ArrayList<double []> lista_pos_destino = calcularPosicionesDestino(lista_posiciones.size() - 1, m, X_dest, Y_dest, offset);
        
       
        System.out.println("Tamaño lista: " + vt.size());
        int sz = vt.size();
        for(int i = 0; i < sz; i++) {
            vt.removeLast();
            aux.removeLast();
        }
        System.out.println("Tamaño lista: " + vt.size());
        
        
        Term a = new NumberTermImpl(1);
        Term b = new NumberTermImpl(2);
        
        
        
        for(int i = 0; i < lista_pos_destino.size(); i++) {
                a = new NumberTermImpl(lista_pos_destino.get(i)[0]);
                b = new NumberTermImpl(lista_pos_destino.get(i)[1]);
                
                aux.append(a);
                aux.append(b);
            
            
            
        }
        
        
        
        
        System.out.println(aux);
        
      
       
        
        ObjectTermImpl type = new  ObjectTermImpl(lista_pos_destino);
    
       
        return un.unifies(aux, args[4]);   // TODO: ARREGLAR EL RETURN
    }
    double pendiente(double [] pos_lider, double X_dest, double Y_dest) {
        
        double X_lider = pos_lider[0];
        double Y_lider = pos_lider[1];
        
        double m = (Y_dest - Y_lider) / (X_dest - X_lider);
        //System.out.println("MiPosicion es: X:" + X_lider + " Y:" + Y_lider + " ... Hay que ir a: "+ X_dest + " "+ Y_dest + " y la pendiente es: " + (-1/m));
        
        //devolvemos la pendiente de la recta perpendicular
        return -(1/m);
    }
    
    ArrayList<double []> calcularPosicionesDestino(int n, double m, double X_dest, double Y_dest,double offset) {
        
        ArrayList<double []> lista_pos_destino = new ArrayList<double []>();
        
        double a, b, c, auxX, auxY;
       
        
        for(int i = 0; i < n ; i++) {
            double [] auxiliar = new double[2];
            double [] auxiliar2 = new double[2];
             
            a = (1 + m*m);
            b = -2*X_dest - 2*X_dest*m*m;
            c = X_dest*X_dest*(1 + m*m) - offset*offset;
            
            // //System.out.println("A: " + a + " B: " + b + " C: " +c );           
            auxX = (-b + Math.sqrt(b*b - 4*a*c))/ (2*a);
            auxY = m*(auxX - X_dest) + Y_dest;
            
            auxiliar[0] = auxX;
            auxiliar[1] = auxY;
            
            lista_pos_destino.add(auxiliar);
            //System.out.println(auxX + " " + auxY + " ");
            i++;
            
            if (i == n)
                break;
            
            auxX = (-b - Math.sqrt(b*b - 4*a*c))/ (2*a);
            auxY = m*(auxX - X_dest) + Y_dest;
            
            auxiliar2[0] = auxX;
            auxiliar2[1] = auxY;
            
            lista_pos_destino.add(auxiliar2);
            //System.out.println(auxX + " " + auxY + " ");
            
            offset = offset + 10;
            
            
        }

    
    
        return lista_pos_destino;
    }
    
    double[] getPos(String pos){
		pos = pos.replaceAll("[^\\.0123456789]","-");
		
		String[] tokens = pos.split("-");
		double[] res = new double[2];
		int i = 0;
		for(String str: tokens){
			if(!str.isEmpty()){
				if(Double.parseDouble(str) != 0){ // es la X-> 0
					res[i] = Double.parseDouble(str);
					i++;
				}
				
			}
			
			
		}
		return res;
	}

    
}
